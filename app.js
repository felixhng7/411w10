const Express = require("express");
const { graphqlHTTP } = require('express-graphql');const Mongoose = require("mongoose");
const {
    GraphQLID,
    GraphQLString,
    GraphQLList,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLInt,
    GraphQLFloat
} = require("graphql");

var app = Express();

Mongoose.connect('mongodb://localhost/Telecomunication', { useNewUrlParser: true, useUnifiedTopology: true })

var logsSchema = new Mongoose.Schema({
    loadClass: {
        Avg: Number,
        Min: Number,
        Max: Number,
        Std_dev: Number,
        Count: Number
    },
    searchClasspath: {
        Avg: Number,
        Min: Number,
        Max: Number,
        Std_dev: Number,
        Count: Number
    },
    searchFile: {
        Avg: Number,
        Min: Number,
        Max: Number,
        Std_dev: Number,
        Count: Number
    },
    replaceExtendedMetaClasses: {
        Avg: Number,
        Min: Number,
        Max: Number,
        Std_dev: Number,
        Count: Number
    },
    ViewRepositoryClientImpl: {
        Avg: Number,
        Min: Number,
        Max: Number,
        Std_dev: Number,
        Count: Number
    },
    ViewRepositoryImpl: {
        Avg: Number,
        Min: Number,
        Max: Number,
        Std_dev: Number,
        Count: Number
    },
    WebJARS: {
        Avg: Number,
        Min: Number,
        Max: Number,
        Std_dev: Number,
        Count: Number
    },
    startDate: String,
    endData: String
});

var logsModel = Mongoose.model('411W9', logsSchema)

const infosType = new GraphQLObjectType({
    name: "infos",
    fields: {
        Avg: { type: GraphQLFloat },
        Min: { type: GraphQLFloat },
        Max: { type: GraphQLFloat },
        Std_dev: { type: GraphQLFloat },
        Count: { type: GraphQLInt }
    }
});


const logsType = new GraphQLObjectType({
    name: "Logs",
    fields: {
        id: { type: GraphQLID },
        loadClass: { type: infosType },
        searchClasspath: { type: infosType },
        searchFile: { type: infosType },
        replaceExtendedMetaClasses: { type: infosType },
        ViewRepositoryClientImpl: { type: infosType },
        ViewRepositoryImpl: { type: infosType },
        WebJARS: { type: infosType },
        startDate: { type: GraphQLString },
        endDate: { type: GraphQLString }
    }
});

const schema = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: "Query",
        fields: {
            logs: {
                type: GraphQLList(logsType),
                resolve: (root, args, context, info) => {
                    return logsModel.find().exec();
                }
            }
        }
    })
});

app.use("/graphql", graphqlHTTP({
    schema: schema,
    graphiql: true
}));

app.listen(3000, () => {
    console.log("Listening at :3000...");
});
